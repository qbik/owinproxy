using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;

namespace HelloWeb
{
    public class Startup
    {
        private string fwd = "https://www.google.pl";
        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles();
            app.Run(async ctx =>
            {
                ProcessRequest(ctx);
                await Task.Yield();
            });
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                HttpResponse response = context.Response;

                // Check for query string
                //string uri = Uri.UnescapeDataString(context.Request.QueryString.ToString());
                //if (string.IsNullOrWhiteSpace(uri))
                //{
                //    response.StatusCode = 403;
                //    return;
                //}

                var query = context.Request.QueryString.Value;
                var path = context.Request.Path.Value;
                if (context.Request.Query.ContainsKey("fwd"))
                {
                    fwd = context.Request.Query["fwd"];
                    query = query.Replace("fwd=" + fwd, "").Replace("&&", "&");
                    if (query == "?") query = "";
                    Console.Write("setting fwd to: {0}", fwd);
                }

                var uri = new UriBuilder(fwd)
                {
                    Query = query,
                };
                uri.Path = uri.Path.TrimStart('/') + "/" + context.Request.Path.Value.TrimStart('/');




                Console.WriteLine("========================");
                Console.WriteLine("=====> [{1}] {0}", uri.Uri, context.Request.Method);

                //// Filter requests
                //if (!uri.ToLowerInvariant().Contains("wikimedia.org"))
                //{
                //    response.StatusCode = 403;
                //    return;
                //}

                // Create web request
                var webRequest = (HttpWebRequest)HttpWebRequest.Create(uri.Uri);
                string ssl = null;

                ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) =>
                {
                    ssl = certificate.ToString();
                    return true;
                };

                ServicePointManager.ClientCipherSuitesCallback += (SecurityProtocolType p, IEnumerable<string> allCiphers) =>
                {
                    //string prefix = p == SecurityProtocolType.Tls ? "TLS_" : "SSL_";
                    //return new List<string> { prefix + "RSA_WITH_AES_128_CBC_SHA", prefix + "RSA_WITH_AES_256_CBC_SHA" };
                    Console.WriteLine("i know these ciphers: {0}", string.Join(",", allCiphers));
                    return
                        allCiphers;
                };

                Console.WriteLine("current security protocol: {0}", ServicePointManager.SecurityProtocol);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                Console.WriteLine("setting security protocol to: {0}", ServicePointManager.SecurityProtocol);


                webRequest.Method = context.Request.Method;
                foreach (var h in context.Request.Headers)
                {
                    try
                    {
                        var name = h.Key;
                        var val = string.Join(";", h.Value);
                        // Console.WriteLine("-> [{0}]: {1}", name, val);
                        //webRequest.Headers.Add(name: name, value: val);
                    }
                    catch
                    {
                        //ignore
                    }
                }

                webRequest.Accept = "*/*";
                webRequest.UserAgent = "curl/7.35.0";
                webRequest.Host = uri.Host;
                foreach (var name in webRequest.Headers.AllKeys)
                {
                    try
                    {
                        var val = webRequest.Headers.Get(name);
                        //context.Response.Headers.Add(name, new string[] { val });
                        Console.WriteLine("-> [{0}]: {1}", name, val);
                    }
                    catch
                    {
                        //ignore
                    }
                }


                // Send the request to the server
                WebResponse serverResponse = null;
                HttpWebResponse httpResponse = null;
                try
                {
                    serverResponse = webRequest.GetResponse();
                    httpResponse = (HttpWebResponse)serverResponse;

                }
                catch (WebException webExc)
                {

                    Console.WriteLine("!!!!!!!!");
                    Console.WriteLine("!! [{1}] error Status={0}", webExc.Status, uri);
                    Console.WriteLine(webExc.Message);


                    try
                    {
                        var resp = (HttpWebResponse)webExc.Response;
                        response.StatusCode = (int)resp.StatusCode;
                        Console.WriteLine("got: ({0}) {1}", (int)resp.StatusCode, resp.StatusDescription);
                    }
                    catch
                    {
                        response.StatusCode = 500;
                    }
                    Console.WriteLine("!!!!!!!!");

                    //response.StatusCode = webExc.Response.;
                    webExc.Response.GetResponseStream().CopyTo(response.Body);
                    return;
                }

                Console.WriteLine("<----- Status: {0} {1}", httpResponse.StatusCode, httpResponse.StatusDescription);

                if (ssl != null)
                {
                    Console.WriteLine("<- SSL: {0}", ssl.Replace("\r\n", " "));
                }

                foreach (var name in serverResponse.Headers.AllKeys)
                {
                    try
                    {
                        var val = serverResponse.Headers.Get(name);
                        context.Response.Headers.Add(name, new string[] { val });
                        Console.WriteLine("<- [{0}]: {1}", name, val);
                    }
                    catch
                    {
                        //ignore
                    }
                }

                // Exit if invalid response
                if (serverResponse == null)
                {
                    return;
                }

                // Configure reponse
                response.ContentType = serverResponse.ContentType;
                response.StatusCode = (int)httpResponse.StatusCode;
                Stream stream = serverResponse.GetResponseStream();

                byte[] buffer = new byte[32768];
                int read = 0;

                int chunk;
                while ((chunk = stream.Read(buffer, read, buffer.Length - read)) > 0)
                {
                    read += chunk;
                    if (read != buffer.Length)
                    {
                        continue;
                    }
                    int nextByte = stream.ReadByte();
                    if (nextByte == -1)
                    {
                        break;
                    }

                    // Resize the buffer
                    byte[] newBuffer = new byte[buffer.Length * 2];
                    Array.Copy(buffer, newBuffer, buffer.Length);
                    newBuffer[read] = (byte)nextByte;
                    buffer = newBuffer;
                    read++;
                }

                // Buffer is now too big. Shrink it.
                byte[] ret = new byte[read];
                Array.Copy(buffer, ret, read);

                response.Body.Write(ret, 0, ret.Length);
                serverResponse.Close();
                stream.Close();

                Console.WriteLine("========================");
            }
            catch (Exception ex)
            {
                Console.WriteLine("!!!!!!!!");
                for (var inner = ex; inner != null; inner = inner.InnerException)
                {
                    Console.WriteLine("!! [{1}] {0}", inner.Message, inner.GetType().Namespace);
                    Console.WriteLine("!! {0}", inner.StackTrace);
                }

                Console.WriteLine("!!!!!!!!");
            }
        }
        public bool IsReusable
        {
            get { return false; }
        }
    }
}
